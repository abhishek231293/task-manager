const express = require("express");
const router = express.Router();
const auth = require("../middlewares/auth");
const Task = require('../models/task');

router.post('/tasks', auth, async (req, res) => {

    // const task = new Task(req.body);

    const task = new Task({
        ...req.body,
        'user_id': req.user._id
    })

    try{
        await task.save()
        res.status(201).send(task);
    }catch(error){
        res.status(400).send(error);
    }

});

router.get('/tasks', auth, async (req, res)=>{

    try{
        // const tasks = await Task.find({'user_id': req.user._id})
        // const match = {...req.query}

        const match = {}
        const sort = {}

        const limit = (req.query && req.query.limit) ? parseInt(req.query.limit): 10;
        const skip = (req.query && req.query.skip) ? parseInt(req.query.skip): 0;

        if(req.query && req.query.completed){
            match.completed = req.query.completed === 'true'
        }

        if(req.query && req.query.title){
            match.title = { $regex: '.*' + req.query.title + '.*' } ;
        }

        if(req.query.sortBy){
            const parts = req.query.sortBy
        }

       await req.user.populate({
                                path: 'tasks',
                                match,
                                options:{
                                    limit,
                                    skip
                                }
                              }).execPopulate();
        
        res.send(req.user.tasks);
    }catch(error){
        res.status(500).send(error);
    }

})

router.get('/tasks/:id', auth, async (req, res)=>{
    
    const _id = req.params.id;

    try{

        const tasks = await Task.findOne({_id, 'user_id': req.user._id})

        if(!tasks){
            return res.status(404).send('No Task found');
        }

        res.send(tasks);

    }catch(error){
        res.status(500).send(error);
    }

})


router.patch('/tasks/:id', auth, async (req, res)=>{

    const _id = req.params.id;

    const updates = Object.keys(req.body);
    const allowedUpdates = ['title', 'description', 'completed'];
    const isValidUpdate = updates.every((update) => allowedUpdates.includes(update));

    if(!isValidUpdate){
        return res.status(400).send({error:"Invalid Updates"});
    }

    try{
        const task = await Task.findOne({_id, 'user_id': req.user._id});

        if(task){
            updates.forEach((update)=>task[update] = req.body[update])

            await task.save();
            res.send(task);
        }else{
            return res.status(404).send('No Tasks found to update');
        }

    }catch(error){
        res.status(500).send(error);
    }

})


router.delete('/tasks/:id', auth, async (req, res)=>{

    const _id = req.params.id;

    try{

        const tasks = await Task.findOneAndDelete({_id, 'user_id': req.user._id})

        if(!tasks){
            return res.status(404).send({error:"Task not found to delete"})
        }

        res.send(tasks);


    }catch(error){
        res.status(500).send(error);    
    }

})

module.exports = router;
