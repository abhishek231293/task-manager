const express = require('express');
const sharp = require("sharp")
const multer = require('multer');
const User = require('../models/user');
const {sendWelcomeEmail, sendCancellationEmail} = require('../email/account')
const auth = require("../middlewares/auth");
const router = new express.Router();

router.post('/users', async (req, res)=>{

    const user = new User(req.body);

    try{
        await user.save();
        sendWelcomeEmail(user.email, user.name)
        const token = await user.generateAuthToken();
        res.status(201).send({user, token});
    }catch(error){
        res.status(400).send('Error: '+ error);
    }

});

router.post('/users/login', async (req, res)=>{

    try{
        const user = await User.findUserByCredential(req.body);
        const token = await user.generateAuthToken();
        res.send({user, token});
    }catch(error){
        res.status(400).send('Error: '+ error);
    }

})

router.post('/users/logout', auth, async (req, res)=>{

    try{

        req.user.tokens = req.user.tokens.filter((token)=> req.token !== token.token)

        await req.user.save()

        res.send('Loged out successfully')
    }catch(error){
        res.status(400).send('Error: '+ error);
    }

})

router.post('/users/logoutAll', auth, async (req, res)=>{

    try{
        req.user.tokens = [];
        await req.user.save();

        res.send('Loged out from all devices')
    }catch(error){
        res.status(400).send('Error: '+ error);
    }
    

})

router.get('/users', auth, async (req, res) => {

    try{
        const user = req.user;
        res.send(user);
    }catch(error){
        res.status(500).send(error);
    }

});

router.patch('/users', auth, async (req, res) => {
    
    const _id = req.user._id;
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password', 'age'];

    const isValidUpdate = updates.every((update) => allowedUpdates.includes(update));

    if(!isValidUpdate){
        return res.status(400).send({error: "Invalid update request"});
    }

    try{

        // const user = await User.findById({_id});
        updates.forEach((update)=>req.user[update] = req.body[update]);
        await req.user.save();
        
        return res.send(req.user);
        
    }catch(error){
        res.status(500).send(error);

    }

})

//Delete user own account
router.delete('/users', auth, async (req, res)=>{

    try{

        req.user.remove()
        sendCancellationEmail(req.user.email, req.user.name)
        res.send(req.user)

    }catch(error){
        res.status(500).send(error);
    }

})

const upload = multer({
    // dest:'./public/profile_image',
    limits:{
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {

        if(!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg)$/)){
            cb(new Error('Please upload PNG/JPG/JPEG Image only'))
        }

        cb(undefined, true)
    }
})

router.post('/users/me/avtar', auth, upload.single('profile_image'), async (req, res)=>{
    
    try{
        const buffer = await sharp(req.file.buffer).resize({ width: 250, height:250 }).png().toBuffer();
        req.user.profile_pic = buffer
        await req.user.save()
        res.send(req.user);
    }catch(error){
        res.status(500).send(error);
    }
   

}, (error, req, res, next)=>{

    res.status(400).send('Error: ' + error.message)

})

router.delete('/users/me/avatar', auth, async (req, res)=>{

    try{

        req.user.profile_pic = null
        await req.user.save();
    
        res.send({message:'Profile pic deleted successfully', data: req.user})

    }catch(error){
        res.status(500).send(error);
    }

})

router.get('/users/avatar', auth, async (req, res)=>{

    try{

        if(req.user.profile_pic){
            res.set('content-type', 'image/png');
            res.send(req.user.profile_pic)
        }else{
            res.send('Image not found');
        }
        

    }catch(error){
        res.status(500).send(error);
    }

})

module.exports = router;