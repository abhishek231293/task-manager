require('dotenv').config()

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {

  const msg = {
    to: email,
    from: process.env.EMAIL_FROM,
    subject: 'Welcome to Task Manager App',
    html: `Welcome  to the app, ${name}. <br/>Now you can manage your task using this App`,
  };

  sgMail.send(msg).then((result)=>{
    return true
  }).catch((err)=>{
    return false    
  });

}

const sendCancellationEmail = (email, name) => {

  const msg = {
    to: email,
    from: process.env.EMAIL_FROM,
    subject: 'Cancellation Task Manager App',
    html: `Hi ${name}, <br/>Your account has been deleted from the app.<br/>Hope to see you back sometime soon.`,
  };

  sgMail.send(msg).then((result)=>{
    return true
  }).catch((err)=>{
    return false    
  });

}

module.exports = {
  'sendWelcomeEmail': sendWelcomeEmail,
  'sendCancellationEmail': sendCancellationEmail
}