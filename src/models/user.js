const mongoose = require('mongoose');
const validator = require('validator');
require('dotenv').config()
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const Task = require('./task')

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'User name required'],
        trim:true
    },
    email:{
        type: String,
        required: [true, 'Email id required'],
        trim:true,
        unique: true,
        lowercase:true,
        validate(value){

            if(!validator.isEmail(value)){
                throw new Error('Invalid email')
            }

        }
    },
    password:{
        type: String,
        required: [true, 'Password is required'],
        trim: true,
        minlength: [7, 'Min 7 character required'],
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('Password should not contain password')
            }
        }
    },
    age:{
        type: Number,
        default:0,
        validate(value){
            if(value < 0){
                throw new Error('Age must be a positive number')
            }
        }
    },
    tokens: [{
        token:{
            type: String,
            require:true
        }
    }],
    profile_pic:{
        type: Buffer
    }
}, {
    timestamps: true
})

//Relationship between User and Task
userSchema.virtual('tasks', {
    ref:'Task',         
    localField: '_id', 
    foreignField: 'user_id' 
})

//To hide password and all tokens in response
userSchema.methods.toJSON = function(){

    const user = this

    const userObj = user.toObject()

    delete userObj.password
    delete userObj.tokens
    delete userObj.profile_pic

    return userObj

}

userSchema.methods.generateAuthToken = async function(){

    const user = this;

    const token = jwt.sign({_id:user._id.toString()}, process.env.JWT_SECRET)

    user.tokens = user.tokens.concat({token})
    await user.save()

    return token

}

userSchema.statics.findUserByCredential = async({email, password}) => {

    const user = await User.findOne({email});

    if(!user){
        throw new Error('unable to login')
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if(!isMatch){
        throw new Error('unable to login')
    }

    return user;

}


userSchema.pre('save',async function(next){
    const user = this;

    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password, 8)
    }
    next();
})

userSchema.pre('remove', async function(next){

    const user = this
    await Task.deleteMany({'user_id':  user._id})
    next()

})

const User = mongoose.model('User', userSchema);

User.pre

module.exports = User;