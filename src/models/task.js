const mongoose = require('mongoose');
const validator = require('validator');

const taskSchema = new mongoose.Schema({
    'title': {
        type: String,
        required: [true, 'Title is required'],
        trim: true
    },
    'description': {
        type:String,
        required: [true, 'Description is required'],
        trim: true
    },
    'completed':{
        type: Boolean,
        default: false
    },
    'user_id': {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
}, {
    timestamps: true
});

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;